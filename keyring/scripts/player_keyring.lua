local ui = require('openmw.ui')
local self = require('openmw.self')
local core = require('openmw.core')
local types = require('openmw.types')
local nearby = require('openmw.nearby')
local storage = require('openmw.storage')

local time = 0
local doOnce = false

local keyring = nil

local modStorage = storage.playerSection('openmw_keyring')


local function onLoad()
	keyring = modStorage:asTable()["keyring"]
end

local function onSave()
	modStorage:set("keyring", keyring)

end

local function event_addKey(keyName)
	keyring[keyName] = true
end

local function event_hasKey(data)
	if keyring[data.keyName] == true then
		core.sendGlobalEvent("event_unlock", data.doorOrContainer)
	end
end

return {
		engineHandlers = { onLoad = onLoad, onSave = onSave },
		eventHandlers = {event_addKey = event_addKey, event_hasKey = event_hasKey}
	}

