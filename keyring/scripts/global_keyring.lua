local core = require('openmw.core')
local types = require("openmw.types")
local world = require("openmw.world")
local async = require('openmw.async')


local function createKeyringDraft()
	local misc = {
		icon = "m/Tx_key_standard_01.tga", 
--		id = "gnounc_keyring", 
		isKey = false, 
		model = "meshes/m/Key_Standard_01.NIF", 
		name = "keyring", 
		value = 12, 
		weight = 0.5
	}

	return types.Miscellaneous.createRecordDraft(misc)
end

local function createKeyring(draft)
	local record = world.createRecord(draft)
	return world.createObject(record.id) 
end

local function getKeyring(player)
	local inv = types.Actor.inventory(player):getAll(types.Miscellaneous)

	for _, item in ipairs(inv) do
		local record = item.type.record(item)
		if record.name == "keyring" then
			return item
		end
	end

	return nil
end


--if player does not have a keyring, give them a keyring
local function onPlayerAdded(player)
	local keyring = getKeyring(player)

	if keyring == nil then
		local draft = createKeyringDraft()
		keyring = createKeyring(draft)
		keyring:moveInto(types.Actor.inventory(player))
	end
end

local function event_unlock(object)
	core.sound.playSoundFile3d("soundFx/trans/chain_pul2.wav", object)

	async:newUnsavableSimulationTimer(0.15, function()  
		types.Lockable.unlock(object)
	end)
end

local function onActivate(object, actor)

	local keyring = getKeyring(actor)

	if keyring == nil then return end

	if object.type.record(object).isKey == true then
		local keyName = object.type.record(object).name
		actor:sendEvent("event_addKey", keyName)

		--wait for player to be able to add the key to the keyring, then kill the actual key.
		async:newUnsavableSimulationTimer(0.15, function()
			object:remove()
		end)
	end

	if object.type == types.Door or object.type == types.Container then
		local keyRecord = types.Lockable.getKeyRecord(object)

		if keyRecord == nil then 
			return 
		else
		end

		actor:sendEvent("event_hasKey", {doorOrContainer = object, keyName = keyRecord.name})
	end

end

return { 
	engineHandlers = { onPlayerAdded = onPlayerAdded, onActivate = onActivate },
	eventHandlers = {event_unlock = event_unlock }
}


